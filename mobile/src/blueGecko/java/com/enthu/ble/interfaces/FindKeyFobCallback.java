package com.enthu.ble.interfaces;

import com.enthu.ble.ble.BluetoothDeviceInfo;

public interface FindKeyFobCallback {
    void findKeyFob(BluetoothDeviceInfo fob);

    void triggerDisconnect();

    String getDeviceName();
}
