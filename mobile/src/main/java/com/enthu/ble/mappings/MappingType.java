package com.enthu.ble.mappings;

public enum MappingType {
    SERVICE,
    CHARACTERISTIC
}
