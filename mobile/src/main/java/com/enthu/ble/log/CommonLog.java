package com.enthu.ble.log;

import com.enthu.ble.other.LogType;

public class CommonLog extends Log {

    public CommonLog(String value, String deviceAddress) {
        setLogTime(getTime());
        setLogInfo(value);
        setLogType(LogType.INFO); //malo wazne
        setDeviceAddress(deviceAddress);
    }
}
