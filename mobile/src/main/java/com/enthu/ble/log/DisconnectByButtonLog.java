package com.enthu.ble.log;

import com.enthu.ble.other.LogType;

public class DisconnectByButtonLog extends Log {

    public DisconnectByButtonLog(String deviceAddress) {
        setLogTime(getTime());
        setLogInfo(deviceAddress + " Disconnected on UI");
        setLogType(LogType.INFO); //malo wazne
        setDeviceAddress(deviceAddress);
    }

}