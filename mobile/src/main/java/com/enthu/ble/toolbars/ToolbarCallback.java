package com.enthu.ble.toolbars;

import com.enthu.ble.utils.FilterDeviceParams;

public interface ToolbarCallback {
    void close();

    void submit(FilterDeviceParams filterDeviceParams, boolean close);

}
