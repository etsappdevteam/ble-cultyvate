package com.enthu.ble.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.enthu.ble.Dashboard;
import com.enthu.ble.dialogs.ProgressDialogWithSpinner;
import com.enthu.ble.menu.MenuItem;
import com.enthu.ble.menu.MenuItemType;
import com.enthu.ble.R;
import com.enthu.ble.adapters.MenuAdapter;

import java.util.ArrayList;
import java.util.Locale;




public class DevelopFragment extends Fragment implements MenuAdapter.OnMenuItemClickListener {

    private ArrayList<MenuItem> list;
    private MenuAdapter.OnMenuItemClickListener menuItemClickListener;

    private Activity view;
    private ProgressDialogWithSpinner dialogInterface;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        //lang
//        Button changelang = view.findViewById(R.id.lang);
//        changelang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showChangeLanguageDialog();
//            }
//        });

        // Here prepare all menu items you want to display in Develop view
        list = new ArrayList<>();
        list.add(new MenuItem(R.drawable.bt_strong,
                getResources().getString(R.string.title_Browser),
                getResources().getString(R.string.description_Browser),

                MenuItemType.BLUETOOTH_BROWSER));
    }

//    ///lang
//    private void showChangeLanguageDialog() {
//        final String[] Listitems = {"English","Hindi"};
//        AlertDialog.Builder mbuilder = new AlertDialog.Builder(getContext());
//        mbuilder.setTitle("Choose Language...");
//        mbuilder.setSingleChoiceItems(Listitems, -1, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int i) {
//                if (i==0){
//                    setLocacale("en");
//                    recreate();
//                }
//                if (i==0){
//                    setLocacale("hi-rIN");
//                    recreate();
//                }
//                dialogInterface.dismiss();
//            }
//        });
//            AlertDialog mDialog = mbuilder.create();
//            mDialog.show();
//    }
//
//    private void setLocacale(String lang) {
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBasecontext().getResources().updateConfiguration(config, getBasecontext().getResources().getDisplayMetrices());
//        SharedPreferences.Editor editor= getSharedPreferences("settings", MODE_PRIVATE).edit();
//        editor.putString("My_Lang",lang);
//        editor.apply();
//    }
//
//    public void Loadlocale(){
//        SharedPreferences prefs = getSharedPreferences("settings", Activity.MODE_PRIVATE);
//        String Language = prefs.getString("My_Lang", "");
//        setLocacale(Language);
//    }
//
////    private SharedPreferences.Editor getSharedPreferences(String settings) {
////    }
////
////    private Fragment getBasecontext() {
////    }
////
////    private void recreate() {
////    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            menuItemClickListener = (MenuAdapter.OnMenuItemClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_develop, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recylerview_develop_menu);
        MenuAdapter adapter = new MenuAdapter(list, this, getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(adapter);

        return view;




    }


    @Override
    public void onMenuItemClick(MenuItemType menuItemType) {
        menuItemClickListener.onMenuItemClick(menuItemType);
    }
}
