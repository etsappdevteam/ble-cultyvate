package com.enthu.ble.interfaces;

import com.enthu.ble.mappings.Mapping;

public interface MappingCallback {
    void onNameChanged(Mapping mapping);
}
