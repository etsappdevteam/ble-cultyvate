package com.enthu.ble.interfaces;

import com.enthu.ble.ble.BluetoothDeviceInfo;

public interface ServicesConnectionsCallback {
    void onDisconnectClicked(BluetoothDeviceInfo deviceInfo);

    void onDeviceClicked(BluetoothDeviceInfo device);
}
