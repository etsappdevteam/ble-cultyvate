package com.enthu.ble.utils;

public enum ToolbarName {
    CONNECTIONS, LOGS, FILTER
}
